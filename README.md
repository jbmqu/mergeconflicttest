# README #

Welcome! This is a repository with two text files (and a readme) which is intended to server as a quick test for your git merge skills. If you would like to try out you git merge capabilities, then for the repository and have a go at merging different branches to see what you get.

In this repository, here are the following branches (with dependencies)
````
-- master
     |
     --test-setup
          |
          - test-setup-b1-file1edit
          - test-setup-b2-file2edit
          - test-setup-b3-file1delete
          - test-setup-b4-file1and2delete
          - test-setup-b5-file1and2edit
````

## Challenges ##
Fork the repository and then clone a version of that fork to your local machine (or just clone the repository to your local machine... you won't be able to push changes though). 
Then, try to attemt each of the challenges locally (without pushing back to your upstream fork).

1. merge test-setup-b1-file1edit into test-setup-b2-file2edit
2. merge test-setup-b1-file1edit into test-setup-b5-file1and2edit
3. merge test-setup-b2-file2edit into test-setup-b3-file1delete
4. merge test-setup-b1-file1edit into test-setup-b3-file1delete
5. merge test-setup-b1-file1edit into test-setup-b4-file1and2delete

Some of these challenges will cause merge conflicts, while other ones will not cause merge conflicts.

## Some Advice ##
1. make sure you have your username and email set up in your git config
2. make sure you have preferences changed to use your preferred editor when dealing with merge conflicts
